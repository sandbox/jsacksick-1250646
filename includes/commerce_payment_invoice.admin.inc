<?php

/**
 * @file
 * Administrative forms for the Payment by invoice module.
 */

/**
 * Form callback: allows the user to change the status of a payment by invoice.
 */
function commerce_payment_invoice_form($form, &$form_state, $order, $transaction) {
  $form_state['order'] = $order;
  $form_state['transaction'] = $transaction;

  // Load and store the payment method instance for this transaction.
  $payment_method = commerce_payment_method_instance_load($transaction->instance_id);
  $form_state['payment_method'] = $payment_method;

  $form['status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#description' => t('Update the status of the payment'),
    '#default_value' => $transaction->status,
    '#options' => _commerce_payment_invoice_statuses(),
  );

  $form = confirm_form($form,
    t('Are you sure you want to update the status ?'),
    'admin/commerce/orders/' . $order->order_id . '/payment',
    '',
    t('Update'),
    t('Cancel'),
    'confirm'
  );
  return $form;
}

/**
 * Return an array containing the payment statuses.
 *
 * @return array
 */
function _commerce_payment_invoice_statuses() {
  $statuses = commerce_payment_transaction_statuses();
  $options_statuses = array();

  // Creating an array with statuses options.
  foreach ($statuses as $key => $val) {
    $options_statuses[$key] = $val['title'];
  }

  return $options_statuses;
}

/**
 * Submit handler: update the payment status.
 */
function commerce_payment_invoice_form_submit($form, &$form_state) {
  $options_statuses = _commerce_payment_invoice_statuses();

  // Retrieve the transaction.
  $transaction = $form_state['transaction'];

  // Check if the status exists.
  if (isset($options_statuses[$form_state['values']['status']])) {
    $transaction->status = $form_state['values']['status'];
  }
  // Save the updated transaction.
  commerce_payment_transaction_save($transaction);
  drupal_set_message(t('Status updated successfully.'));

  // Redirecting.
  $form_state['redirect'] = 'admin/commerce/orders/' . $form_state['order']->order_id . '/payment';
}
